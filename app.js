require('dotenv').config();

const express = require('express');
const parser = require('body-parser');
const app = express();
const Eticket = require('./service/eticket');
const Qismo = require('./service/qismo');
const port = process.env.PORT || 3000;

app.use(parser.json());
app.get('/', (req, res) => {
  res.send('Service is working');
});

// Create ticket end-point
app.post('/ticket', async (req, res, next) => {
  try {
    const data = req.body;
    const payload = {};
    payload.subject = `Ticket room ${data.room_id}`;
    payload.description = 'Description Ticket';
    payload.priority = 'low';

    res.sendStatus(200);
    const resp = await Eticket.CreateTicket(payload);
    const ticket_id = resp.result.id;
    const user_info = [{ key: 'ticket_id', value: ticket_id }];
    Qismo.SetUserInfo(data.room_id, user_info);
  } catch (err) {
    next(err);
  }
});

// Resolve ticket
app.post('/resolve', async (req, res, next) => {
  try {
    const data = req.body;
    console.log(data);
    const additional_info = data.customer.additional_info;

    let ticket_id = additional_info.filter(val => {
      return val.key == 'ticket_id';
    });

    ticket_id = ticket_id[0].value;
    Eticket.ResolveTicket(ticket_id);

    res.sendStatus(200);
  } catch (err) {
    next(err);
  }
});

app.use((error, req, res, next) => {
  const status = error.status || 500;
  res.status(status).json({
    status: status,
    message: error.message
  });
});

app.listen(port, console.log('App running on port', port));

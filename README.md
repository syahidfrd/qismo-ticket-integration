# Qismo ticket integration

Objective: Can submit tickets to the [ticketing service](https://gitlab.com/syahidfrd/eticket) from the qiscus multichannel submit ticket feature, as well as when resolving.

### Requirements

- Node = 13.7.0
- Npm = 6.13.6

### Setting up Project

- Setup environtment variable. Rename `.env.example` to `.env`.
- Install all project dependencies.

```bash
npm install
```

### How to Run

- Compiles and hot-reloads with nodemon

```bash
npm run dev
```

### Multichannel Setup

- Set the webhook ticket url with this endpoint `base_url/ticket`
- Set the webhook resolves url with this endpoint `base_url/resolve`

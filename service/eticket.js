const axios = require('axios');

class Eticket {
  constructor() {
    this.admin_token = process.env.ADMIN_TOKEN;
    this.base = process.env.ETICKET_URL;
  }

  async CreateTicket(payload) {
    try {
      const url = `${this.base}/ticket`;
      const { data } = await axios.post(url, payload, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: this.admin_token
        }
      });
      console.log('Ticket created...');
      return data;
    } catch (error) {
      throw error;
    }
  }

  async ResolveTicket(ticket_id) {
    try {
      const url = `${this.base}/ticket/resolve`;
      const { data } = await axios.post(
        url,
        { ticket_id: ticket_id },
        {
          headers: {
            Authorization: this.admin_token
          }
        }
      );

      console.log(data);
      return data;
    } catch (err) {
      console.log(err);
    }
  }
}

module.exports = new Eticket();

const axios = require('axios');

class Qismo {
  constructor() {
    this.token = process.env.QISMO_TOKEN;
    this.base_url = process.env.QISMO_BASE_URL;
  }

  async GetUserInfo(room_id) {
    try {
      const url = `${this.base_url}/qiscus/room/${room_id}/user_info`;

      const { data } = await axios.get(url, {
        headers: {
          Authorization: this.token
        }
      });

      const data_resp = data.data;
      let user_info = [];

      if (data_resp != null) {
        const extras = data_resp.extras;
        if (extras != null) {
          const user_properties = extras.user_properties;
          if (user_properties != null) {
            user_info = user_properties;
          }
        }
      }

      return user_info;
    } catch (err) {
      throw err;
    }
  }

  async SetUserInfo(room_id, new_user_info) {
    try {
      console.log(new_user_info);
      const old_user_info = await this.GetUserInfo(room_id);
      const user_info = [...old_user_info, ...new_user_info];

      const url = `${this.base_url}/qiscus/room/${room_id}/user_info`;
      const payload = { user_properties: user_info };

      const { data } = await axios.post(url, payload, {
        headers: {
          Authorization: this.token
        }
      });
      console.log('Set user info');
      return data;
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new Qismo();
